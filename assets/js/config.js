window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
window.Vue = require('vue');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'Csrf-Token': window.Play.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

import 'es6-promise/auto';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale });

import 'font-awesome/scss/font-awesome.scss';
