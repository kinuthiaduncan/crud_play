       # --- !Ups

create table "students" (
  "id" SERIAL not null primary key,
  "admission_number" varchar not null UNIQUE,
  "name" varchar not null,
  "age" int not null,
  "gender" varchar not null
);

# --- !Downs

drop table "students" if exists;