package models

import play.api.libs.json._


case class Student (id: Long, admission_number: String, name: String, age: Int, gender: String)

object Student{
  implicit val studentFormat = Json.format[Student]
}