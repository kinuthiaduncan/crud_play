package models

import javax.inject.{ Inject, Singleton }
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ Future, ExecutionContext }

@Singleton
class StudentRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private class StudentTable(tag: Tag) extends Table[Student](tag, "students") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def admission_number = column[String]("admission_number")

    def name = column[String]("name")

    def age = column[Int]("age")

    def gender = column[String]("gender")

    def * = (id, admission_number, name, age, gender) <> ((Student.apply _).tupled, Student.unapply)
  }

  private val student = TableQuery[StudentTable]

  def create(admission_number: String, name: String, age: Int, gender: String): Future[Student] = db.run {
    (student.map(p => (p.admission_number, p.name, p.age, p.gender))
      returning student.map(_.id)
      into ((admissionNameAgeGender, id) => Student(id, admissionNameAgeGender._1, admissionNameAgeGender._2, admissionNameAgeGender._3, admissionNameAgeGender._4))
      ) += (admission_number, name, age, gender)
  }

  def list(): Future[Seq[Student]] = db.run {
    student.result
  }

  def listOne(admission_number: String): Future[Seq[Student]] = {
    db.run(student.filter(_.admission_number === admission_number).result)
  }

  def update(admission_number: String, name: String, age: Int, gender: String): Future[Int] = db.run(student
    .filter(_.admission_number === admission_number).map(p => (p.admission_number, p.name, p.age, p.gender))
    .update(admission_number, name, age, gender))

  def delete(admission_number: String): Future[Int] = {
    db.run(student.filter(_.admission_number === admission_number).delete)
  }
}
