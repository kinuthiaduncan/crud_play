package controllers

import javax.inject.Inject
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.libs.json.Json
import play.api.mvc.{MessagesAbstractController, MessagesControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

class StudentController @Inject() (cc: MessagesControllerComponents) (repo: StudentRepository)
                                  (implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  val studentForm: Form[CreateStudentForm] = Form {
    mapping(
      "admission_number" -> nonEmptyText,
      "name" -> nonEmptyText,
      "age" -> number.verifying(min(0), max(140)),
      "gender" -> nonEmptyText
    )(CreateStudentForm.apply)(CreateStudentForm.unapply)
  }

  def index = Action { implicit request =>
    Ok(views.html.student(studentForm))
  }


  def storeStudent = Action.async(implicit request => {
    studentForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(Ok(views.html.student(errorForm)))
      },
      student => {
        repo.create(student.admission_number, student.name, student.age, student.gender).map { _ =>
          Ok(Json.obj("status" ->"OK", "message" -> "Student Saved!!" ))
        }
      }
    )
  })

  def getAllStudents = Action.async(implicit request => {
    repo.list().map { students =>
      Ok(Json.toJson(students))
    }
  })

  def getStudent(admission_number: String) = Action.async(implicit request => {
    repo.listOne(admission_number).map {student =>
      Ok(Json.toJson(student))
    }
  })

  def updateStudent = Action.async(implicit request => {
    studentForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(Ok(views.html.student(errorForm)))
      },
      student => {
        repo.update(student.admission_number, student.name, student.age, student.gender).map { _ =>
          Ok(Json.obj("status" ->"OK", "message" -> "Student Updated!!" ))
        }
      }
    )
  })

  def deleteStudent(admission_number: String) = Action.async(implicit request => {
    repo.delete(admission_number).map {_ =>
      Ok(Json.obj("status" ->"OK", "message" -> "Student Deleted!!" ))
    }
  })

}
case class CreateStudentForm(admission_number: String, name: String, age: Int, gender: String)
